package com.epam;

import java.util.Arrays;

public class sorting {
    

    public void sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }else{
            Arrays.sort(array);
        }
    }
    
}
