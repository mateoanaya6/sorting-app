package com.epam;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.xml.sax.SAXException;

@RunWith(Parameterized.class)
public class AppTest {

    private int[] inputArray;
    private int[] expectedArray;

    sorting sorting = new sorting();

    public AppTest(int[] inputArray, int[] expectedArray) {
        this.inputArray = inputArray;
        this.expectedArray = expectedArray;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {                // Null case
            { new int[]{}, new int[]{} },   // Empty case
            { new int[]{1}, new int[]{1} }, // Single element array case
            { new int[]{5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5} },  // Other cases
            { new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5} },  // Already sorted case
            { new int[]{5, 5, 5, 5, 5}, new int[]{5, 5, 5, 5, 5} },  // All elements same case
            { new int[]{5, 4, 3, 2, 1, 6, 7, 8, 9, 10}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} },  // More than ten elements case
            { new int[]{5, 4, 3, 2, 1, 6, 7, 8, 9, 10, 11}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11} }  // More than ten elements case
        });
    }

    @Test (expected = IllegalArgumentException.class)
    public void testNullCase() {
        sorting.sort(null);
    }

    @Test
    public void testSorting() {
        sorting.sort(inputArray);
        assertArrayEquals(expectedArray, inputArray);
    }
}